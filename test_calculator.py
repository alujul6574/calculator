from calculadora import *

def test_suma():
    assert suma(2,3) == 5

def test_resta():
    assert resta(2, 3) == -1

def test_multiplica():
    assert multiplica(2, 3) == 6

def test_divide():
    assert divide(10,5) == 2
