# Calculator

This project is made to test a Jenkins pipeline. The code we are going to use in the pipeline is written in python.

## Instalation

First of all, we need to use Jenkins. In this scenario we will use Jenkins in a docker container. To install Jenkins with docker, we have to create a directory that we will link as a volume when running Jenkins.

```bash
mkdir $HOME/jenkins_home
sudo chown 1000 $HOME/jenkins_home
sudo chmod 777 $HOME/jenkins_home
docker run -d -p 49001:8080 -v $HOME/jenkins_home:/var/jenkins_home --name jenkins jenkins/jenkins:lts-jdk17
```

We visit the url http://localhost:49001/ and it will ask us for the administrator password that can be retrieved with the command docker logs jenkins or by doing a cat of the file $HOME/jenkins_home/secrets/initialAdminPassword

After that, we will install the suggested plugins and then we have to introduce the admin user data.

With these steps we will have the Jenkins istalation completed.

After the Jenkins istallation we have to install python and pytest.

```bash
docker exec -it -u root *container_name* /bin/bash
apt update
apt install -y python3 python3-pytest
```

## Usage

To create our pipeline

- Clic on `New item`
- Choose the name
- Select `Pipeline` and click `Ok`
- Go to the end and change the Definition to `Pipeline script from SCM`
- On SCM select `Git`, Repository URL `https://gitlab.com/alujul6574/calculator.git`, on Branch Specifier write `*/main` and finally, on Script Path write `Jenkinsfile` and save.

Now we have our pipeline created. The last step is to test it clicking on `Build Now` and check the stages.


> Thanks to Vincent Stevenson https://www.youtube.com/watch?v=6njM8g5hKuk
